# -*- encoding : utf-8 -*-
Banyai5::Application.config.middleware.use ExceptionNotification::Rack,
  :email => {
    :email_prefix => "[banyai exception] ",
    :sender_address => %{"notifier" <zoltan@farm.co.hu>},
    :exception_recipients => %w{puskas.eva@thunderies.com zoltan@farm.co.hu}
  }
