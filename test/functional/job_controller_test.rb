# -*- encoding : utf-8 -*-
require 'test_helper'

class JobControllerTest < ActionController::TestCase
  test "should get list" do
    get :list
    assert_response :success
  end

  test "should get show" do
    get :show
    assert_response :success
  end

end
