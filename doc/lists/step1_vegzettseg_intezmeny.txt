ANNYE - Andrássy Gyula Budapesti Német Nyelvű Egyetem
ATF - Adventista Teológiai Főiskola
ÁVF - Általános Vállalkozási Főiskola
AVKF - Apor Vilmos Katolikus Főiskola
BCE - Budapesti Corvinus Egyetem
BGF - Budapesti Gazdasági Főiskola
BHF - Bhaktivedanta Hittudományi Főiskola
BKF - Budapesti Kommunikációs és Üzleti Főiskola
BKTF - Budapest Kortárstánc Főiskola
BME - Budapesti Műszaki és Gazdaságtudományi Egyetem
BTA - Baptista Teológiai Akadémia
DE - Debreceni Egyetem
DF - Dunaújvárosi Főiskola
DRHE - Debreceni Református Hittudományi Egyetem
EDUTUS - Edutus Főiskola
EGHF - Egri Hittudományi Főiskola
EHE - Evangélikus Hittudományi Egyetem
EJF - Eötvös József Főiskola
EKF - Eszterházy Károly Főiskola
ELTE - Eötvös Loránd Tudományegyetem
ESZHF - Esztergomi Hittudományi Főiskola
FU - FernUniversität in Hagen
GDF - Gábor Dénes Főiskola
GFHF - Gál Ferenc Hittudományi Főiskola
GTF - Golgota Teológiai Főiskola
GYHF - Győri Hittudományi Főiskola
IBS - IBS Nemzetközi Üzleti Főiskola
KE - Kaposvári Egyetem
KEE - Közép-európai Egyetem
KF - Kecskeméti Főiskola
KJF - Kodolányi János Főiskola
KRE - Károli Gáspár Református Egyetem
KRF - Károly Róbert Főiskola
LFZE - Liszt Ferenc Zeneművészeti Egyetem
MCDC - McDaniel College
ME - Miskolci Egyetem
MKE - Magyar Képzőművészeti Egyetem
MOME - Moholy-Nagy Művészeti Egyetem
MPANNI - Mozgássérültek Pető András Nevelőképző és Nevelőintézete
MTF - Magyar Táncművészeti Főiskola
NKE - Nemzeti Közszolgálati Egyetem
NYF - Nyíregyházi Főiskola
NYME - Nyugat-magyarországi Egyetem
OBU - Oxford Brookes University International Business School
OE - Óbudai Egyetem
ORZSE - Országos Rabbiképző - Zsidó Egyetem
PE - Pannon Egyetem
PHF - Pécsi Püspöki Hittudományi Főiskola
PPKE - Pázmány Péter Katolikus Egyetem
PRTA - Pápai Református Teológiai Akadémia
PTE - Pécsi Tudományegyetem
PTF - Pünkösdi Teológiai Főiskola
SE - Semmelweis Egyetem
SRTA - Sárospataki Református Teológiai Akadémia
SSTF - Sola Scriptura Teológiai Főiskola
SSZHF - Sapientia Szerzetesi Hittudományi Főiskola
SZAGKHF - Szent Atanáz Görög Katolikus Hittudományi Főiskola
SZBHF - Szent Bernát Hittudományi Főiskola
SZE - Széchenyi István Egyetem
SZF - Szolnoki Főiskola
SZFE - Színház- és Filmművészeti Egyetem
SZIE - Szent István Egyetem
SZPA - Szent Pál Akadémia
SZTE - Szegedi Tudományegyetem
TKBF - A Tan Kapuja Buddhista Főiskola
TPF - Tomori Pál Főiskola
VHF - Veszprémi Érseki Hittudományi Főiskola
VISART - École d'Art Maryse Eloy
WJLF - Wesley János Lelkészképző Főiskola
WSUF - Wekerle Sándor Üzleti Főiskola
ZSKF - Zsigmond Király Főiskola