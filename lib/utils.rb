# -*- encoding : utf-8 -*-
# http://lovehateubuntu.blogspot.hu/2009/01/ruby-arraytohash.html
class Array
  def to_hash
    self.inject({}) { |h, nvp| h[nvp[0]] = nvp[1]; h }
  end
end
