# -*- encoding : utf-8 -*-
ActiveAdmin.register Page do


  index do
    column "id", :sortable => :id do |page|
      link_to page.id, admin_page_path(page)
    end

    column "Title", :sortable => :title do |page|
      link_to page.title, admin_page_path(page)
    end
    column :url
    column :created_at
  end

  form do |f|

    f.inputs "Oldal" do
      f.input :title
      f.input :url
      f.input :body, :as => :ckeditor, :label => false
    end
    f.buttons
  end

  show do
    attributes_table do
      row :title
      row :url
      row (:body) { |foobar| raw(foobar.body) }
    end
  end
end
