# -*- encoding : utf-8 -*-
include ActiveAdmin::ActsAsList::Helper

ActiveAdmin.register Attainment do

  menu :parent => "Címkék"

  # Sort by position
  config.sort_order = 'position'

  # Add member actions for positioning.
  sortable_member_actions

  index do
    # This adds columns for moving up, down, top and bottom.
    sortable_columns

    #...
    column :name
    default_actions
  end  
end
