# -*- encoding : utf-8 -*-
ActiveAdmin.register Applicant do

  filter :email
  filter :first_name_or_last_name, :as => :string, :label => "név"
  filter :country, :as => :select, :collection => proc { banyai_country_options_for_select() }
  filter :city
  filter :language_skills_language_code, 
    :as => :select, :collection => proc { banyai_language_options_for_select() },
    :label => "nyelv"
  filter :educations_educational_institution_name, :as => :select, 
    :collection => EducationalInstitution.all.map(&:name),
    :label => "oktatási intézmény"
  filter :experiences_management_level_name, :as => :select, 
    :collection => ManagementLevel.all.map(&:name), :label => "beosztás tipusa"
  filter :experiences_job_category_name, :as => :select, 
    :collection => JobCategory.all.map(&:name), :label => "állás kategória"
  filter :experiences_job_subcategory_name, :as => :select, 
    :collection => JobSubcategory.all.map(&:name), :label => "állás alkategória"
  filter :experiences_position_name, :as => :select, 
    :collection => Position.all.map(&:name), :label => "pozíció"
  filter :experiences_branch_name, :as => :select, 
    :collection => Branch.all.map(&:name), :label => "ágazat"
  filter :experiences_employer, :as => :string, :label => "munkáltató"


  index do
    column "id", :sortable => :id do |applicant|
      link_to applicant.id, admin_applicant_path(applicant)
    end

    column "Name", :sortable => :last_name do |applicant|
      link_to "#{applicant.last_name} #{applicant.first_name}", admin_applicant_path(applicant)
    end
    column :email
    column :created_at
  end

  form(:html => { :multipart => true }) do |f|
    f.inputs "Regisztrációs alapadatok" do
      f.input :last_name
      f.input :first_name
      f.input :email
      f.input :phone
    end

    f.inputs "Cím, tartózkodás" do
      f.input :country
      f.input :postal_code
      f.input :province_id,  :as => :select, 
        :collection => Province.select_options, 
        :include_blank => true
      f.input :city
    end

    f.inputs "Számítógépes ismeretek" do
      f.input :computer_skills
    end

    f.has_many :language_skills do |j|
      j.input :language_code, :as => :select, 
        :collection =>  banyai_language_options_for_select(j.object.language_code)
      j.input :level, :as => :select, 
        :collection => LanguageSkill::LEVELS
    end

    f.has_many :educations do |j|
      j.input :attainment_id, :as => :select,
        :collection => Attainment.all.collect { |a| [ a.name, a.id ] }
      j.select :educational_institution_id, :as => :select, 
        :collection => EducationalInstitution.all.collect { |a| [ a.name, a.id ] }
      j.input :other_institution
      j.input :name
      j.input :from_year, :as => :select, :collection => 1960..Time.now.year
      j.input :to_year,   :as => :select, :collection => 1960..Time.now.year      
    end

    f.has_many :experiences do |j|
      j.input :management_level_id, :as => :select,
        :collection => ManagementLevel.all.collect { |a| [ a.name, a.id ] }
      j.input :job_category_id, :as => :select, :collection => JobCategory.all.collect { |a| [ a.name, a.id ] },
        :input_html => { :class => "category_select" }, :wrapper_html => { :class => "category_wrapper" }
      j.input :job_subcategory_id, :as => :select,
       :collection => option_groups_from_collection_for_select(JobCategory.all, :job_subcategories, :name, :id, :name, j.object.job_subcategory_id),
       :input_html => { :class => "subcategory_select" }, :wrapper_html => { :class => "subcategory_wrapper" }
      j.input :position_id, :as => :select,
        :collection => Position.all.collect { |a| [ a.name, a.id ] }
      j.input :other_position
      j.input :employer
      j.input :branch_id, :as => :select,
        :collection => Branch.all.collect { |a| [ a.name, a.id ] }
      j.input :from_year, :as => :select, :collection => 1960..Time.now.year
      j.input :to_year,   :as => :select, :collection => 1960..Time.now.year              
    end

    f.inputs "Önéletrajz feltöltés" do
      f.input :cv_hu,  :hint => (f.template.link_file(f.object.cv_hu_url)) 
      f.input :cv_en,  :hint => (f.template.link_file(f.object.cv_en_url)) 
      f.input :cv_etc, :hint => (f.template.link_file(f.object.cv_etc_url)) 
    end


    f.inputs "Feltételek" do
      f.input :accept_terms
      f.input :newsletter
    end

    f.buttons
  end

  show do |a|
    attributes_table do
      row :last_name
      row :first_name
      row :email
      row :phone

      row :country
      row :city
      row :province do
        Province.find(a.province_id)
      end

      row :postal_code

      row :computer_skills

      row "Nyelvek" do
        known = []

        a.language_skills.each do |ls|
          known << "#{ls.language_name} (#{ls.level})"
        end

          div do
            simple_format known.join(", ")
          end
      end

      row :accept_terms
      row :newsletter

      row :cv_hu do
         link_file(a.cv_hu_url)
      end

      row :cv_en do
         link_file(a.cv_en_url)
      end

      row :cv_etc do
         link_file(a.cv_etc_url)
      end

    end


    panel("Végzettség") do
      a.educations.each do |e|
        panel("") do
          attributes_table_for(e) do
            row :attainment
            row :educational_institution
            row :other_institution
            row :name
            row :from_year
            row :to_year
          end
        end
      end
    end

    panel("Szakmai tapasztalat") do
      a.experiences.each_with_index do |e, i|
        panel("") do
          attributes_table_for(e) do
            row :management_level
            row :job_category
            row :job_subcategory
            row :position
            row :other_position
            row :employer
            row :branch
            row :from_year
            row :to_year            
          end
        end
      end
    end

    active_admin_comments
  end

end
