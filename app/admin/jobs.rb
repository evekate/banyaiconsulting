# -*- encoding : utf-8 -*-
ActiveAdmin.register Job do
  
  index do
    column "id", :sortable => :id do |job|
      link_to job.id, admin_job_path(job)
    end

    column "Név", :sortable => :name do |job|
      link_to job.name, admin_job_path(job)
    end
    column :country
    column :city
    column :deadline_on
  end

  form do |f|
    f.inputs "Alapadatok" do
      f.input :name
      f.input :position
      f.input :country, :as => :select, :collection =>  banyai_country_options_for_select(f.object.country)
      f.input :city
      f.input :deadline_on
      f.input :publish_on
      f.input :expire_on
      f.input :branch
      f.input :management_level
      f.input :status, :as => :boolean
    end

    f.inputs "Leírás" do
      f.input :description, :as => :ckeditor, :label => false
    end

      f.has_many :categorizations do |j|
        j.input :job_category_id, :as => :select, :collection => JobCategory.all.collect { |a| [ a.name, a.id ] },
          :input_html => { :class => "category_select" }, :wrapper_html => { :class => "category_wrapper" }
        j.input :job_subcategory_id, :as => :select,
         :collection => option_groups_from_collection_for_select(JobCategory.all, :job_subcategories, :name, :id, :name, j.object.job_subcategory_id),
         :input_html => { :class => "subcategory_select" }, :wrapper_html => { :class => "subcategory_wrapper" }
        if !j.object.nil?
          j.input :_destroy, :as => :boolean, :label => "Törlés"
        end         
      end

      f.has_many :language_skills do |j|
        j.input  :language_code, :as => :select, :collection =>  banyai_language_options_for_select(j.object.language_code)
        j.input :level, :as => :select, :collection => LanguageSkill::LEVELS
        if !j.object.nil?
          j.input :_destroy, :as => :boolean, :label => "Törlés"
        end          
      end

    f.inputs "Cég adatai" do
      f.input :company_name
      f.input :company_description, :as => :ckeditor, :label => false
    end

    f.inputs "Cél" do 
      f.input :goal, :as => :ckeditor, :label => false 
    end

    f.inputs "Fő feladatok/felelősségi körök" do
     f.input :responsibilities, :as => :ckeditor, :label => false
    end

    f.inputs "Képzettség/követelmények" do
      f.input :skills, :as => :ckeditor, :label => false
    end

    f.inputs "Személyes elvárások" do
      f.input :personal_requirements, :as => :ckeditor, :label => false
    end

    f.inputs "Amit nyújtunk" do
      f.input :we_offer, :as => :ckeditor, :label => false
    end

    f.inputs "Jelentkezés módja" do
      f.input :how_to_apply, :as => :ckeditor, :label => false
    end

    f.buttons
  end



  show do |a|
    attributes_table do
      row :name
      row :position
      row :city
      row :country
      row "Jelentkezők" do
          ul do
            a.applicants.each do |e|
                li link_to(e.name, admin_applicant_path(e))
            end
          end
      end

      row "Alkalmas regisztráltak" do
          ul do
            a.scores.first(10).each do |e|
              if e.applicant
                li link_to(e.applicant.name, admin_applicant_path(e.applicant)) + " (#{e.points} pont)"
              end
            end
          end
      end

      row :deadline_on
      row :publish_on
      row :expire_on
      row :branch
      row :management_level
      row :status do
        a.status ? "PUBLIKUS" : "REJTETT"
      end

      row "Kategóriák" do
        ul do 
          a.categorizations.each do |c| 
            li "#{c.job_category.andand.name} / #{c.job_subcategory.andand.name}"
          end
        end
      end

      row :language_skills do
        ul do 
          a.language_skills.each do |l| 
            li "#{l.language_name} (#{l.level})"
          end
        end
      end


      row :description do
        a.description.html_safe
      end

      row :company_name

      row :company_description do
        a.company_description.html_safe
      end

      row :goal do
        a.goal.html_safe
      end

      row :responsibilities do
        a.responsibilities.html_safe
      end

      row :skills do
        a.skills.html_safe
      end
      row :personal_requirements do
        a.personal_requirements.html_safe
      end
      row :we_offer do
        a.we_offer.html_safe
      end
      row :how_to_apply do
        a.how_to_apply.html_safe
      end


    end
    active_admin_comments
  
  end

end
