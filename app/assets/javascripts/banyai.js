// IE console.log protection
try { console.log('init console... done'); } catch(e) { console = { log: function() {} } };

var subcategories = [];

function watch_category_select() {
  $('form').on('change', '.category_select', function() {

    // set global var the first time
    if (subcategories.length < 1) {
      subcategories = $('.subcategory_select').html();
    }
    category = $("option:selected", this).text();
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1');
//    console.log("escaped category");
//    console.log(escaped_category);
    options = $(subcategories).filter("optgroup[label=" + escaped_category + "]").html();
  //  console.log(options);
    sub_wrapper = $(this).parents('.nested-fields').find('.subcategory_wrapper');
    sub_select = sub_wrapper.find('.subcategory_select');

    if (options) {
      sub_select.html(options);
      sub_wrapper.show();
    } else {
      sub_select.empty();
      sub_wrapper.hide();
    }
  });
}

$(document).ready(watch_category_select);

(function() {
  if (typeof window['CKEDITOR_BASEPATH'] === "undefined" || window['CKEDITOR_BASEPATH'] === null) {
    window['CKEDITOR_BASEPATH'] = "/assets/ckeditor/";
  }
}).call(this);


$(function() {
  $("input.file_upload").filestyle({
    image: "/images/btn_tall.png",
    imageheight : 28,
    imagewidth : 87,
  });
}); 


// Append the function to the "document ready" chain
jQuery(function($) {
  // when the #search field changes
  $("#applicant_postal_code").change(function() {
    // make a POST call and replace the content
    $.get('/postal_codes/show', { code: $("#applicant_postal_code").val() }, function(data) {
      if (data) {
        $("#applicant_city").val(data.city);
        $("#applicant_province_id").val(data.province_id);
        $("#applicant_country").val('hu');
      }
//      console.log(data);
    });
  });
})
