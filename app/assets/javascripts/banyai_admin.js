
// ckeditor fix
(function() {
  if (typeof window['CKEDITOR_BASEPATH'] === "undefined" || window['CKEDITOR_BASEPATH'] === null) {
    window['CKEDITOR_BASEPATH'] = "/assets/ckeditor/";
  }
}).call(this);


var subcategories = [];


function init_subcategories() {
  $('form').on('change', '.category_select', function() {

    // set global var the first time
    if (subcategories.length < 1) {
      subcategories = $('.subcategory_select').html();
    }
    category = $("option:selected", this).text();
    escaped_category = category.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1');
  //  console.log("escaped catregory");
  //  console.log(escaped_category);
    options = $(subcategories).filter("optgroup[label=" + escaped_category + "]").html();
  //  console.log(options);
    sub_wrapper = $(this).parents('.has_many_fields').find('.subcategory_wrapper');
    sub_select = sub_wrapper.find('.subcategory_select');

    if (options) {
      sub_select.html(options);
      sub_wrapper.show();
    } else {
      sub_select.empty();
      sub_wrapper.hide();
    }
  });

}

jQuery(document).ready(init_subcategories);