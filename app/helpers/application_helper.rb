# -*- encoding : utf-8 -*-
module ApplicationHelper

  # http://stackoverflow.com/questions/3059704/rails-3-ideal-way-to-set-title-of-pages
  def title(page_title)
    content_for :title, page_title.to_s
  end

  def link_file(url)
    if !url || url.empty?
      return ""
    end
    
    filename = url.split("/").last
    link_to(filename, url)
  end

  def banyai_language_options_for_select(selected = nil, priority_languages = ['en', 'de', 'fr', 'ru', 'it'])
    language_options = "".html_safe

    list = I18nData.languages("HU").invert
    # proper i18n sorting
    list = list.collect { |k, v| [UnicodeUtils.downcase(k), UnicodeUtils.downcase(v)] }
    list = list.sort_alphabetical_by(&:first)
    priority = list.select { |item| priority_languages.include? item.last }
    remaining = list - priority

    if !priority_languages.empty?
      language_options += options_for_select(priority, selected)
      language_options += "<option value=\"\" disabled=\"disabled\">-------------</option>\n".html_safe
      selected=nil if priority_languages.include?(selected)
    end

    return language_options + options_for_select(remaining, selected)
  end

  def banyai_country_options_for_select(selected = nil, priority_countries = ['hu', 'rs', 'hr', 'si', 'at', 'sk', 'ua', 'ro'])
    country_options = "".html_safe

    list = I18nData.countries("HU").invert
    # proper i18n sorting
    # list = list.collect { |k, v| [UnicodeUtils.downcase(k), UnicodeUtils.downcase(v)] }
    list = list.sort_alphabetical_by(&:first)
    priority = list.select { |item| priority_countries.include? item.last }
    remaining = list - priority

    # manual sort
    countries = I18nData.countries("HU")
    priority = [
      [countries['HU'], 'HU'], 
      [countries['AT'], 'AT'], 
      [countries['HR'], 'HR'], 
      [countries['RO'], 'RO'], 
      [countries['RS'], 'RS'], 
      [countries['SK'], 'SK'], 
      [countries['UA'], 'UA']
    ]

    if !priority_countries.empty?
      country_options += options_for_select(priority, selected)
      country_options += "<option value=\"\" disabled=\"disabled\">-------------</option>\n".html_safe
      selected=nil if priority_countries.include?(selected)
    end

    return country_options + options_for_select(remaining, selected)
  end

end
