# -*- encoding : utf-8 -*-
module ProfileHelper

  def resource_name
    :applicant
  end

  def resource
    @resource ||= Applicant.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:applicant]
  end
  
end
