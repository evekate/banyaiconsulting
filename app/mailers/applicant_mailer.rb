# -*- encoding : utf-8 -*-
class ApplicantMailer < ActionMailer::Base
  default :from => "info@banyaiconsulting.hu"

  def welcome_email(applicant)
    @applicant = applicant
    mail(:to => applicant.email, :subject => "Sikeres regisztráció - banyaiconsulting.hu")
  end
end
