# -*- encoding : utf-8 -*-
# https://github.com/plataformatec/devise/wiki/How-To:-Redirect-to-a-specific-page-on-successful-sign-up-(registration)
class RegistrationsController < Devise::RegistrationsController
  # GET /resource/sign_up
  def new
    #if verify_recaptcha
      resource = build_resource({})

      # have an empty one to start with
      resource.language_skills.build
      resource.educations.build

      respond_with resource
    #else
      #flash.delete :recaptcha_error
      #flash[:alert] = "Validate that you are Human"
      
    #end
  end

  def create
    if !verify_recaptcha
      flash.delete :recaptcha_error
      build_resource(sign_up_params)
      resource.valid?
      resource.errors.add(:base, "There was an error with the recaptcha code below. Please re-enter the code.")
      clean_up_passwords(resource)
      respond_with_navigational(resource) { render :new }
    else
      flash.delete :recaptcha_error
      super
    end
  end

  protected

  def after_sign_up_path_for(resource)
    profile_step2_path
  end

  

end
