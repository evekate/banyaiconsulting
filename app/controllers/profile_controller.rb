# -*- encoding : utf-8 -*-
class ProfileController < ApplicationController

  def edit
    @resource = current_applicant
  end

  def update
      @resource = Applicant.find(current_applicant.id)
      if @resource.update_attributes(params[:applicant])
          # Sign in the user bypassing validation in case his password changed
          sign_in @resource, :bypass => true
          redirect_to profile_step2_path
      else
        render "edit"
      end
  end

  def step_2
    @resource = current_applicant
    @resource.experiences.build
  end

  def step2_update
    @resource = Applicant.find(current_applicant.id)
      if @resource.update_attributes(params[:applicant])
        # Sign in the user bypassing validation in case his password changed
        sign_in @resource, :bypass => true
        redirect_to profile_step3_path
      else
        render "step_2"
      end
  end

  def step_3
    @resource = current_applicant
  end

  def step3_update
      @resource = Applicant.find(current_applicant.id)
      if @resource.update_attributes(params[:applicant])
        # Sign in the user bypassing validation in case his password changed
        sign_in @resource, :bypass => true
        redirect_to root_path
      else
        render "step3"
      end
  end


#  def edit
#    @resource = current_applicant
#  end

  
end
