# -*- encoding : utf-8 -*-
class ApplicationController < ActionController::Base
  protect_from_forgery

  before_filter :set_locale

  def after_sign_up_path_for(resource)
    profile_step2_path(resource)
  end

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end


end
