# -*- encoding : utf-8 -*-
class PageNotFound < StandardError; end

class PageController < ApplicationController

  # https://gist.github.com/1322962
  rescue_from PageNotFound do
    render '404.html.erb', :status => :not_found and return false
  end

  def show
    @page = Page.find_by_url(params[:page])
    raise PageNotFound unless @page
  end
end
