# -*- encoding : utf-8 -*-
class JobsController < ApplicationController

  before_filter :authenticate_applicant!, :only => [:apply]

  def index
    @q = Job.active.search(params[:q])
    @jobs=Job.order('publish_on DESC')
    @jobs = @jobs.page(params[:page])
  end

  def show
    @q = Job.search(params[:q])
    @job = Job.find(params[:id])
  end

  def apply
    @q = Job.search(params[:q])    
    @job = Job.find(params[:id])
    current_applicant.jobs << @job
    current_applicant.save
  end
end
