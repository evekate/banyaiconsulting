# -*- encoding : utf-8 -*-
class PostalCodesController < ApplicationController

 respond_to :json, :xml
 def show
    @code = PostalCode.get(params[:code])
    respond_with(@code)
 end

end
