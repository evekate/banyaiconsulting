# -*- encoding : utf-8 -*-
class Applicant < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  after_create :send_welcome_mail

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me,
    :first_name, :last_name, :phone, :country, :postal_code, :city,
    :computer_skills, :cv_hu, :cv_en, :cv_etc, :accept_terms, :newsletter,
    :educations_attributes, :experiences_attributes, :language_skills_attributes,
    :province_id
  # attr_accessible :title, :body

  validates :first_name, :presence => true
  validates :last_name,  :presence => true
  validates :accept_terms,  :presence => true

  has_many :language_skills, :as => :owner
  has_many :educations
  has_many :experiences
  has_many :job_categories, :through => :experiences
  has_many :job_subcategories, :through => :experiences
  has_many :branches, :through => :experiences
  has_many :management_levels, :through => :experiences

  accepts_nested_attributes_for :language_skills, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :educations, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :experiences, :reject_if => :all_blank, :allow_destroy => true

  has_many :signups
  has_many :jobs, :through => :signups

  mount_uploader :cv_hu,  CvUploader
  mount_uploader :cv_en,  CvUploader
  mount_uploader :cv_etc, CvUploader

  after_save :recalculate_score

  def name
    "#{last_name} #{first_name}"
  end

  def has_language_skill(job_ls)
    self.language_skills.each do |my_ls|
      logger.debug("Comparing #{my_ls.language_code} == #{job_ls.language_code}")
      if (my_ls.language_code == job_ls.language_code) && (my_ls.level_value >= job_ls.level_value)
        return true
      end
    end
    return false
  end

  def self.recent(num=5)
    self.order("created_at DESC").first(num)
  end


private
  
  def recalculate_score
    Score.delete_all(:applicant_id => self.id)
    Job.active.each do |j|
      Score.calculate(j, self)
    end
  end

  def send_welcome_mail
    begin
     ApplicantMailer.welcome_email(self).deliver
    rescue
      logger.debug("Couldn't send email")
    end
  end

end
