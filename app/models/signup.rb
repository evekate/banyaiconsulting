# -*- encoding : utf-8 -*-
class Signup < ActiveRecord::Base
  attr_accessible :applicant_id, :job_id

  belongs_to :applicant
  belongs_to :job


  def self.recent(num=5)
    self.order("created_at DESC").first(num)
  end

end
