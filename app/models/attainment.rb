# -*- encoding : utf-8 -*-
class Attainment < ActiveRecord::Base
  attr_accessible :name, :position
  validates :name, :presence => true
  
  acts_as_list
  default_scope :order => 'position ASC'
end
