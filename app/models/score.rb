# -*- encoding : utf-8 -*-
class Score < ActiveRecord::Base
  attr_accessible :applicant_id, :job_id, :points

  belongs_to :applicant
  belongs_to :job

  def self.recalculate_all
    Score.delete_all

    Job.active.each do |j|
      Applicant.find(:all).each do |a|
        Score.calculate(j, a)
      end
    end
 end

 def self.calculate(j, a)
   points = 0

#    logger.debug("=========== Matching job #{j.id} to applicant #{a.id}")
    if j.city && a.city && (j.city.downcase == a.city.downcase)
      points += 2
    end

#    logger.debug("City match, points: #{points}")

    if j.country && a.country && (j.country.downcase == a.country.downcase)
      points += 1
    end

#    logger.debug("Country match, points: #{points}")


    common_categories = a.job_categories & j.job_categories
    points += common_categories.count * 2

#    logger.debug("Category match, points: #{points}")


    common_subcategories = a.job_subcategories & j.job_subcategories
    points += common_subcategories.count * 3

    if a.management_levels.include?(j.management_level)
      points += 1 * 2
    end

    if a.branches.include?(j.branch)
      points += 1 * 5
    end


#    logger.debug("Subcategory match, points: #{points}")



    j.language_skills.each do |ls|
      if a.has_language_skill(ls)
        points += 3
#        logger.debug("Language match, points: #{points}")
      end
    end

    if points > 0
      Score.create(:applicant_id => a.id, :job_id => j.id, :points => points)
    end
 end

end
