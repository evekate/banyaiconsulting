# -*- encoding : utf-8 -*-

class Province

  PROVINCES = ['Bács-Kiskun', 'Baranya', 'Békés', 'Borsod-Abaúj-Zemplén', "Budapest", 'Csongrád', 'Fejér', "Győr-Moson-Sopron", "Hajdú-Bihar",
    "Heves", "Jász-Nagykun-Szolnok", "Komárom-Esztergom", "Nógrád",
    "Pest", "Somogy", "Szabolcs-Szatmár-Bereg", "Tolna", "Vas", "Veszprém",
    "Zala"]

  def self.find(id)
    if id.nil? 
      return true
    end
    
    return self::PROVINCES[id]
  end

  def self.select_options
    self::PROVINCES.each_with_index.map { |p,i| [p, i] }
  end

end
