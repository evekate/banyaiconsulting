# -*- encoding : utf-8 -*-
class Education < ActiveRecord::Base
  attr_accessible :applicant_id, :attainment_id, :educational_institution_id, 
    :name, :from_year, :to_year, :other_institution

#  validates :applicant, :presence => true
  validates :attainment, :presence => true
  validates :educational_institution, :presence => true
  validates :name, :presence => true
  
  belongs_to :applicant
  belongs_to :attainment
  belongs_to :educational_institution
end
