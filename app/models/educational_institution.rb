# -*- encoding : utf-8 -*-
class EducationalInstitution < ActiveRecord::Base
  attr_accessible :code, :name

  validates :name, :presence => true
  validates :code, :uniqueness => true

  default_scope :order => 'name ASC'
end
