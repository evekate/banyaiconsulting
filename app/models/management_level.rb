# -*- encoding : utf-8 -*-
class ManagementLevel < ActiveRecord::Base
  attr_accessible :name, :position
  validates :name, :presence => true
  
  acts_as_list
  default_scope :order => 'position ASC'

  # categories currently assigned to active jobs
  def self.with_jobs
    ManagementLevel.where("id IN (SELECT DISTINCT management_level_id FROM jobs j 
      WHERE j.status = 1 AND j.publish_ON < NOW() AND j.expire_on > NOW())")
  end

end
