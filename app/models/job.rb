# -*- encoding : utf-8 -*-
class Job < ActiveRecord::Base
  attr_accessible :branch_id, :city, :company_description, :company_name, :country, :deadline_on,
    :description, :goal, :how_to_apply, :job_category_id, :name, :personal_requirements,
    :position, :responsibilities, :skills, :we_offer, :status, :categorizations_attributes, :language_skills_attributes,
    :publish_on, :expire_on, :management_level_id

  belongs_to :job_category
  belongs_to :branch
  belongs_to :management_level

  has_many :language_skills, :as => :owner
  has_many :categorizations, :as => :owner
  has_many :job_categories, :through => :categorizations
  has_many :job_subcategories, :through => :categorizations

  accepts_nested_attributes_for :language_skills, :reject_if => :all_blank, :allow_destroy => true
  accepts_nested_attributes_for :categorizations, :reject_if => :all_blank, :allow_destroy => true

  has_many :signups
  has_many :applicants, :through => :signups

  has_many :scores, :order => 'points DESC'
  has_many :suitable_applicants, :through => :scores,
    :order => 'scores.points DESC', :source => :applicant

  scope :active, lambda { where(:status => 1).
    where("publish_on < ?", Time.now ).
    where("expire_on > ?", Time.now ).
    order("publish_on DESC")  }

  paginates_per 10

  before_save :generate_index
  after_save :recalculate_score


  def self.recent(num=5)
    self.active.order("publish_on DESC").first(num)
  end

  def self.cities
    Job.connection.select_values("SELECT DISTINCT city FROM jobs j
      WHERE j.status = 1 AND j.publish_ON < NOW() AND j.expire_on > NOW()
      ORDER BY city")
  end

  def self.positions
    Job.connection.select_values("SELECT DISTINCT position FROM jobs j
      WHERE j.status = 1 AND j.publish_ON < NOW() AND j.expire_on > NOW()
      ORDER BY position")
  end

  def country_name
    countries = I18nData.countries("HU")
    countries[country.upcase]
  end

  # concat all text fields for poor man's fulltext search
  def all_text
    fields = [name, city, branch.name, description, goal, how_to_apply,
      company_name, company_description, personal_requirements, position, responsibilities,
      skills, we_offer]
    fields.map { |f| ActionController::Base.helpers.strip_tags(f) }.join(" ");
  end

  def allas_kategoria
    categorizations.first.andand.job_subcategory.andand.name
  end

  def uzleti_szegmens
    allas_kategoria
  end

private

  def generate_index
    self.search_index = all_text
  end

  def recalculate_score
    Score.delete_all(:job_id => self.id)
    Applicant.find(:all).each do |a|
      Score.calculate(self, a)
    end
  end

end
