# -*- encoding : utf-8 -*-
class JobCategory < ActiveRecord::Base

  set_primary_key :id
  
  attr_accessible :name
  validates :name, :presence => true

  has_many :job_subcategories
  has_many :jobs

  # categories currently assigned to active jobs
  def self.with_jobs
    JobCategory.where("id IN (SELECT DISTINCT c.job_category_id FROM categorizations c
      INNER JOIN jobs j ON c.owner_id = j.id AND c.owner_type='Job'
      WHERE j.status = 1 AND j.publish_ON < NOW() AND j.expire_on > NOW())")
  end

end
