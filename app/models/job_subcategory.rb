# -*- encoding : utf-8 -*-
class JobSubcategory < ActiveRecord::Base
  attr_accessible :job_category_id, :name

  belongs_to :job_category
  has_many :experiences


  # categories currently assigned to active jobs
  def self.with_jobs
    JobSubcategory.where("id IN (SELECT DISTINCT c.job_subcategory_id FROM categorizations c
      INNER JOIN jobs j ON c.owner_id = j.id AND c.owner_type='Job'
      WHERE j.status = 1 AND j.publish_ON < NOW() AND j.expire_on > NOW())")
  end
end
