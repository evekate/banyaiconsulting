# -*- encoding : utf-8 -*-
class Categorization < ActiveRecord::Base
  attr_accessible :job_category_id, :job_subcategory_id, :owner_type, :owner_id

  belongs_to :owner, :polymorphic => true
  belongs_to :job_category
  belongs_to :job_subcategory

end
