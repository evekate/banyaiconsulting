# -*- encoding : utf-8 -*-
class Position < ActiveRecord::Base
  attr_accessible :name
  validates :name, :presence => true

  default_scope :order => 'name ASC'

end
