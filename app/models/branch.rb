# -*- encoding : utf-8 -*-
class Branch < ActiveRecord::Base
  attr_accessible :name
  validates :name, :presence => true

  has_many :jobs
end
