# -*- encoding : utf-8 -*-
class Ckeditor::Asset < ActiveRecord::Base
  include Ckeditor::Orm::ActiveRecord::AssetBase
              
  delegate :url, :current_path, :size, :content_type, :filename, :to => :data
  
  validates_presence_of :data

  # drif hack: avoid NoMethodError: undefined method `content_length' for nil:NilClass
  def size
    return data_file_size
  end

end
