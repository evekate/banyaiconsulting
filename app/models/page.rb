# -*- encoding : utf-8 -*-
class Page < ActiveRecord::Base
  attr_accessible :body, :title, :url
end
