# -*- encoding : utf-8 -*-
class Experience < ActiveRecord::Base
  attr_accessible :applicant_id, :branch_id, :employer, :job_category_id, :job_subcategory_id, :management_level_id, 
    :other_position, :position_id, :from_year, :to_year

#  validates :applicant, :presence => true

  belongs_to :applicant
  belongs_to :branch
  belongs_to :job_category
  belongs_to :job_subcategory
  belongs_to :management_level
  belongs_to :position
end
