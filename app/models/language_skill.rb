# -*- encoding : utf-8 -*-
class LanguageSkill < ActiveRecord::Base
  attr_accessible :owner_id, :owner_type, :language_code, :level

#  validates :applicant_id, :presence => true
  validates :language_code, :presence => true
  validates :level, :presence => true

  LEVELS = ['A1', 'A2', 'B1', 'B2', 'C1', 'C2']
  LEVEL_VALUES = { 'A1' => 1, 'A2' => 2, 
    'B1' => 3, 'B2' => 4, 'C1' => 5, 'C2' => 6 }

  belongs_to :owner, :polymorphic => true

  def language_name
    languages = I18nData.languages("HU")
    languages[language_code.upcase]
  end

  def level_value
    LanguageSkill::LEVEL_VALUES[level]
  end

  # languages currently assigned to active jobs
  def self.with_jobs
    codes = LanguageSkill.connection.select_values("SELECT DISTINCT language_code 
      FROM language_skills ls
      INNER JOIN jobs j ON ls.owner_id = j.id AND ls.owner_type = 'Job'
      ORDER BY language_code")

    languages = I18nData.languages("HU")
    codes.map { |c| [languages[c.upcase], c] }
  end


end
