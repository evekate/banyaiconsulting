# -*- encoding : utf-8 -*-
class PostalCode < ActiveRecord::Base
  # attr_accessible :title, :body

  def self.get(code)
    PostalCode.find_by_postal_code(code.to_s)
  end

  
end
