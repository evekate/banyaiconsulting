# -*- encoding : utf-8 -*-
class AddOfferToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :we_offer, :text
  end
end
