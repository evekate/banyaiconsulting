# -*- encoding : utf-8 -*-
class AddManagementLevelToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :management_level_id, :integer
  end
end
