# -*- encoding : utf-8 -*-
class CreateExperiences < ActiveRecord::Migration
  def change
    create_table :experiences do |t|
      t.integer :applicant_id
      t.integer :management_level_id
      t.integer :job_category_id
      t.integer :position_id
      t.string :other_position
      t.string :employer
      t.integer :branch_id

      t.timestamps
    end
  end
end
