# -*- encoding : utf-8 -*-
class AddProvinceToApplicants < ActiveRecord::Migration
  def change
    add_column :applicants, :province_id, :integer
  end
end
