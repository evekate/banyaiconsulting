# -*- encoding : utf-8 -*-
class AddPolymorphismToLanguageSkills < ActiveRecord::Migration
  def change
    add_column :language_skills, :owner_id, :integer
    add_column :language_skills, :owner_type, :string
    remove_column :language_skills, :applicant_id
  end
end
