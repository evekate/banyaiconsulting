# -*- encoding : utf-8 -*-
class AddOtherToEducations < ActiveRecord::Migration
  def change
    add_column :educations, :other_institution, :string
  end
end
