# -*- encoding : utf-8 -*-
class CreateScores < ActiveRecord::Migration
  def change
    create_table :scores do |t|
      t.integer :job_id
      t.integer :applicant_id
      t.integer :points

      t.timestamps
    end
  end
end
