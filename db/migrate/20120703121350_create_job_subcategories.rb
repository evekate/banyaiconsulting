# -*- encoding : utf-8 -*-
class CreateJobSubcategories < ActiveRecord::Migration
  def change
    create_table :job_subcategories do |t|
      t.integer :job_category_id
      t.string :name

      t.timestamps
    end
  end
end
