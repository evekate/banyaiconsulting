# -*- encoding : utf-8 -*-
class AddYearsToExperiences < ActiveRecord::Migration
  def change
    add_column :experiences, :from_year, :integer
    add_column :experiences, :to_year, :integer
  end
end
