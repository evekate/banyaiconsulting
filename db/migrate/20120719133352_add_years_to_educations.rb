# -*- encoding : utf-8 -*-
class AddYearsToEducations < ActiveRecord::Migration
  def change
    add_column :educations, :from_year, :integer
    add_column :educations, :to_year, :integer
  end
end
