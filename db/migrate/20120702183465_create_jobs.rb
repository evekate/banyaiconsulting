# -*- encoding : utf-8 -*-
class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :name
      t.string :position
      t.string :city
      t.string :country
      t.date :deadline_on
      t.text :description
      t.string :company_name
      t.text :company_description
      t.text :goal
      t.text :responsibilities
      t.text :skills
      t.text :personal_requirements
      t.text :how_to_apply
      t.integer :branch_id
      t.integer :job_category_id

      t.timestamps
    end
  end
end
