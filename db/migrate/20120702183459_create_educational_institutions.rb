# -*- encoding : utf-8 -*-
class CreateEducationalInstitutions < ActiveRecord::Migration
  def change
    create_table :educational_institutions do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
