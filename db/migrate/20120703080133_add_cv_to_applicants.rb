# -*- encoding : utf-8 -*-
class AddCvToApplicants < ActiveRecord::Migration
  def change
    add_column :applicants, :cv_hu, :string
    add_column :applicants, :cv_en, :string
    add_column :applicants, :cv_etc, :string
  end
end
