# -*- encoding : utf-8 -*-
class AddSearchIndexToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :search_index, :text
  end
end
