# -*- encoding : utf-8 -*-
class AddDetailsToApplicants < ActiveRecord::Migration
  def change
    add_column :applicants, :last_name, :string
    add_column :applicants, :first_name, :string
    add_column :applicants, :phone, :string
    add_column :applicants, :country, :string
    add_column :applicants, :postal_code, :string
    add_column :applicants, :city, :string
    add_column :applicants, :computer_skills, :text
    add_column :applicants, :accept_terms, :boolean
    add_column :applicants, :newsletter, :boolean
  end
end
