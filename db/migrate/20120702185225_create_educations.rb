# -*- encoding : utf-8 -*-
class CreateEducations < ActiveRecord::Migration
  def change
    create_table :educations do |t|
      t.integer :applicant_id
      t.integer :attainment_id
      t.integer :educational_institution_id
      t.string :name

      t.timestamps
    end
  end
end
