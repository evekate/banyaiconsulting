# -*- encoding : utf-8 -*-
class AddOwnerTypeToCategorizations < ActiveRecord::Migration
  def change
    add_column :categorizations, :owner_type, :string
    remove_column :categorizations, :owner
  end
end
