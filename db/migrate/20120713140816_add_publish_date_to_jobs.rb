# -*- encoding : utf-8 -*-
class AddPublishDateToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :publish_on, :date
    add_column :jobs, :expire_on, :date
  end
end
