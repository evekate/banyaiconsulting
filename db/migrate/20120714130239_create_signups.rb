# -*- encoding : utf-8 -*-
class CreateSignups < ActiveRecord::Migration
  def change
    create_table :signups do |t|
      t.integer :applicant_id
      t.integer :job_id

      t.timestamps
    end
  end
end
