# -*- encoding : utf-8 -*-
class CreateLanguageSkills < ActiveRecord::Migration
  def change
    create_table :language_skills do |t|
      t.string :applicant_id
      t.string :language_code
      t.string :level

      t.timestamps
    end
  end
end
