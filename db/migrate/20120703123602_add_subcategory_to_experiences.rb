# -*- encoding : utf-8 -*-
class AddSubcategoryToExperiences < ActiveRecord::Migration
  def change
    add_column :experiences, :job_subcategory_id, :integer
  end
end
