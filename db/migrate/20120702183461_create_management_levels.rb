# -*- encoding : utf-8 -*-
class CreateManagementLevels < ActiveRecord::Migration
  def change
    create_table :management_levels do |t|
      t.string :name
      t.string :position

      t.timestamps
    end
  end
end
