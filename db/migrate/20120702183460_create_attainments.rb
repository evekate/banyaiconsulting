# -*- encoding : utf-8 -*-
class CreateAttainments < ActiveRecord::Migration
  def change
    create_table :attainments do |t|
      t.string :name
      t.integer :position

      t.timestamps
    end
  end
end
