# -*- encoding : utf-8 -*-
class CreateCategorizations < ActiveRecord::Migration
  def change
    create_table :categorizations do |t|
      t.integer :owner_id
      t.string :owner
      t.integer :job_category_id
      t.integer :job_subcategory_id

      t.timestamps
    end
  end
end
