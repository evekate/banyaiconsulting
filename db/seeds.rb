# -*- encoding : utf-8 -*-



category = ''
subcategory = ''
cat_id = 0


JobCategory.delete_all
JobSubcategory.delete_all

file = File.new(Rails.root.join('doc','lists','job_categories.txt'), "r")
while (line = file.gets)

  next if line.strip.empty?

  if line[0].strip.empty? 
    subcategory = line.strip
    puts "SUB: #{subcategory}"
    JobSubcategory.create(name: subcategory, job_category_id: cat_id)

  else 
    category = line.strip
    puts "CAT #{cat_id}: #{category}"
    jobcat = JobCategory.create(name: category)
    cat_id = jobcat.id
  end


end
file.close
